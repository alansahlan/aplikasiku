<?php
     
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::Admin--
 
Route::group(['middleware'=>['web']],function(){
	Route::get('/login',['as'=>'login', 'uses' => 'AuthController@login']);
	Route::post('/handleLogin',['as'=>'handleLogin', 'uses' => 'AuthController@handleLogin']);
	Route::get('/admin',['middleware'=>'auth','as'=>'admin','uses'=>'UsersController@admin']);
	Route::get('/logout',['as'=>'logout','uses'=>'AuthController@logout']);
	Route::resource('users','UsersController',['only' => ['create','store']]);
});

Route::get('down',function(){
    Artisan::call('down');
    return view('welcome');
});

Route::get('/', function(){
	return view('users.home');
});

Route::get('data/dokter',function(){
	return view('rs.datadokter');
});

Route::get('data/kamar',function(){
	return view('rs.datakamar');
});

Route::get('fasilitas',function(){
	return view('rs.fasilitas');
});

Route::get('listroom', 'RoomController@showlist');

Route::get('check/room','RoomController@show'); 

Route::get('reservasi/{kamar_list}','RoomController@reservroom');

Route::post('data/reservasi','RoomController@reserv');

Route::get('show/reservasi','RoomController@showreservasi');

Route::get('show/room', 'RsController@showroom');

Route::post('data/kamar','RsController@insertdatakamar');

Route::get('jadwal','RsController@infodr');

Route::controller('tes','TestController');

Route::get('tespdf','PdfController@getPdf');


/*
Route::get('entry','DataController@entry');
Route::get('masuk','DataController@pemasukan');
Route::get('keluar','DataController@pengeluaran');
Route::get('laporan/in','DataController@laporanin');
Route::get('hapus/{id}','DataController@hapus');
Route::post('data/insert','DataController@insert');
Route::get('read',function(){
	return view('read');
});
Route::get('/reservasii', 'AuthController@login');
//Route::get('show/room', 'RsController@showroom');
Route::get('sapa/delete/{id}', 'SapaController@delete');
*/
//Route::Member--
//Route::get('/check/{r}',function($r){
//	return view('check.'.$r);
//});
