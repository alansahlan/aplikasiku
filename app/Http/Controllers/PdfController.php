<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Reservasi;

use PDF;

class PdfController extends Controller
{
    public function getPdf(){
    	$reservasi = Reservasi::all();
    	$pdf = PDF::loadView('pdf.reserv');
    	return $pdf->stream('reserv.pdf');
    }
}
