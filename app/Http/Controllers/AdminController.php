<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\aplikasi;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect; 
use App\Kamar;
use DB;

class AdminController extends Controller {
    public $Response;

	public $crumb;

	public $model;

	public $heads = array();

	public $fields = array();

	public $collection;

	public $controller_name;

	public $form;

	public $form_framework = 'TwitterBootstrap3';

	public $form_class = 'form-horizontal';

	public $validator = array();

	public $actions = '';

	public $form_add = 'new';

	public $form_edit = 'edit';

	public $view_object = 'view';

	public $title = '';

    public $ajaxsource = null;

    public $addurl = null;

    public $importurl = null;

    public $importkey = null;

    public $rowdetail = null;

    public $delurl = null;

    public $dlxl = null;

    public $newbutton = null;

    public $backlink = '';

    public $printlink = '';

    public $pdflink = '';

    public $xlslink = '';

    public $makeActions = 'makeActions';

    public $can_add = true;

    public $can_import = true;

    public $can_export = true;

    public $is_report = false;

    public $report_action = '';

    public $is_additional_action = false;

    public $additional_action = '';

    public $additional_filter = '';

    public $js_additional_param = '';

    public $table_raw = '';

    public $table_dnd = false;

    public $table_dnd_url = '';

    public $table_dnd_idx = 0;

    public $table_group = false;

    public $table_group_field = '';

    public $table_group_idx = 0;

    public $table_group_collapsible = false;

    public $additional_query = false;

    public $modal_sets = '';

    public $js_table_event = '';

    public $def_order_by = 'lastUpdate';

    public $def_order_dir = 'desc';

    public $place_action = 'both'; // first, both

    public $show_select = true; // first, both

    public $additional_page_data = array();

    public $table_view = 'tables.simple';

    public $report_view = 'tables.report';

    public $report_data = null;

    public $report_file_path = null;

    public $report_file_name = null;

    public $report_type = false;

    public $report_entity = false;

    public $doc_number = false;

    public $additional_table_param = array();
    //public $product_info_url = 'ajax/productinfo';

    public $sql_connection = '';

    public $sql_table_name = '';

    public $sql_key = 'id';

    public $product_info_url = null;

    public $prefix = null;

    public $no_paging = false;

    public $aux_data = null;

    public $column_styles = '';

    public $responder_type = 's';

    public $print = false;

    public $pdf = false;

    public $xls = false;

    public $import_main_form = 'shared.importinput';

    public $import_aux_form = '';

    public $export_output_fields = null;

    public $report_filter_input = null;

    public $import_validate_list = null;

    public $import_commit_submit = null;

    public $import_update_exclusion = array();

    public $import_commit_url = null;

    public $search_fields = null;

	public function __construct(){

		$this->title = 'TestPage';

	}


	/**
	 * Setup the layout used by the controller.
	 *
	 /* @return void
	 */
    
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
/*
    public function getIndex()
    {


        Breadcrumbs::addCrumb($this->title,URL::to('/'));

        $controller_name = strtolower($this->controller_name);

        $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
        Event::fire('log.a',array($controller_name, 'view list' ,$actor,'OK'));

        //$this->can_add = false;

        return $this->pageGenerator();
    }

    public function getPrintfile($_id)
    {
        $file = Document::find($_id);
        if($file){
            $body = file_get_contents($file->fullpath);
            $controller_name = strtolower($this->controller_name);
            $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
            Event::fire('log.a',array($controller_name, 'print file content' ,$actor,'OK'));
            print $body;
        }else{
            return $this->get_not_found_page();
        }

    }

    public function getPdffile($_id)
    {

        $file = Document::find($_id);
        if($file){
            $body = file_get_contents($file->fullpath);
            $controller_name = strtolower($this->controller_name);
            $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
            Event::fire('log.a',array($controller_name, 'print file content' ,$actor,'OK'));

            return PDF::loadHTML($body)->setPaper('a4')
                     ->setOrientation('landscape')
                     ->setOption('margin-bottom', 0)
                     ->stream( str_replace('html', 'pdf', $file->filename ) );

        }else{
            return $this->get_not_found_page();
        }


        //return $this->printPage();
    }


    public function getPrint()
    {

        $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
        Event::fire('log.a',array($controller_name, 'print page' ,$actor,'OK'));

        return $this->printPage();
    }

    public function postIndex()
    {

        return $this->tableResponder();
    }

    public function postSQLIndex()
    {
        return $this->SQLtableResponder();
    }

    public function printGenerator()
    {


        Breadcrumbs::addCrumb($this->title,URL::to('/'));

        $controller_name = strtolower($this->controller_name);

        $this->print = true;
        $this->no_paging = true;

        if($this->responder_type == 's'){
            $table = $this->SQLtableResponder();
        }else{
            $table = $this->tableResponder();
        }

        $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
        Event::fire('log.a',array($controller_name, 'view static list' ,$actor,'OK'));


        $this->table_view = 'print.table';

        $this->table_raw = $table;

        //print_r($table);

        return $this->pageGenerator();
    }

    public function printPage()
    {


        $controller_name = strtolower($this->controller_name);

        $this->print = true;
        $this->no_paging = true;

        if($this->responder_type == 's'){
            $table = $this->SQLtableResponder();
        }else{
            $table = $this->tableResponder();
        }

        $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
        Event::fire('log.a',array($controller_name, 'view static list' ,$actor,'OK'));


        $this->table_view = 'print.plain';

        $this->table_raw = $table;

        //print_r($table);

        return $this->pageGenerator();
    }

    public function printThrough($_id)
    {

        $doc = Document::find($_id);


        $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
        Event::fire('log.a',array($controller_name, 'print through document' ,$actor,'OK'));

        if($doc){
            $content = file_get_contents($doc->fullpath);
            print $content;
        }else{
            return View::make('shared.notfound');
        }
    }

    public function printReport()
    {

        $controller_name = strtolower($this->controller_name);

        $actor = (isset(Auth::user()->email))?Auth::user()->fullname.' - '.Auth::user()->email:'guest';
        Event::fire('log.a',array($controller_name, 'view static list' ,$actor,'OK'));

        $this->report_view = 'print.report';

        return $this->reportPageGenerator();
    }

	public function pageGenerator(){

		//$action_selection = Former::select( Config::get('kickstart.actionselection'))->name('action');

		$heads = $this->heads;
        $fields = $this->fields;

        $this->ajaxsource = (is_null($this->ajaxsource))? strtolower($this->controller_name): $this->ajaxsource;

        $this->addurl = (is_null($this->addurl))? strtolower($this->controller_name).'/add': $this->addurl;

        $this->importurl = (is_null($this->importurl))? strtolower($this->controller_name).'/import': $this->importurl;

        $this->rowdetail = (is_null($this->rowdetail))? strtolower($this->controller_name).'.rowdetail': $this->rowdetail;

        $this->delurl = (is_null($this->delurl))? strtolower($this->controller_name).'/del': $this->delurl;

        $this->newbutton = (is_null($this->newbutton))? Str::singular($this->controller_name): $this->newbutton;

        //dialog related url
        //$this->product_info_url = (is_null($this->product_info_url))? strtolower($this->controller_name).'/info': $this->product_info_url;

        $this->prefix = (is_null($this->prefix))? strtolower($this->controller_name):$this->prefix;

		$select_all = Former::checkbox()->name('All')->check(false)->id('select_all');

		// add selector and sequence columns
        $start_index = -1;
        if($this->place_action == 'both' || $this->place_action == 'first'){
            array_unshift($heads, array('Actions',array('sort'=>false,'clear'=>true,'class'=>'action')));
            array_unshift($fields, array('',array('sort'=>false,'clear'=>true,'class'=>'action')));
        }
        if($this->show_select == true){
            array_unshift($heads, array($select_all,array('sort'=>false)));
            array_unshift($fields, array('',array('sort'=>false)));
        }else{
            $start_index = $start_index + 1;
        }
		array_unshift($heads, array('#',array('sort'=>false)));

        array_unshift($fields, array('',array('sort'=>false)));

		// add action column
        if($this->place_action == 'both'){
            array_push($heads,
                array('Actions',array('search'=>false,'sort'=>false,'clear'=>true,'class'=>'action'))
            );
            array_push($fields,
                array('',array('search'=>false,'sort'=>false,'clear'=>true,'class'=>'action'))
            );
        }

		$disablesort = array();

		for($s = 0; $s < count($heads);$s++){
			if($heads[$s][1]['sort'] == false){
				$disablesort[] = $s;
			}
		}

		$disablesort = implode(',',$disablesort);

        /* additional features */



/*

        $this->dlxl = (is_null($this->dlxl))? strtolower($this->controller_name).'/dlxl': $this->dlxl;


        $this->printlink = (is_null($this->printlink) || $this->printlink == '')? strtolower($this->controller_name).'/static': $this->printlink;

        //print_r($this->table_raw);

		return View::make($this->table_view)
			->with('title',$this->title )
			->with('newbutton', $this->newbutton )
			->with('disablesort',$disablesort )
			->with('addurl',$this->addurl )
            ->with('importurl',$this->importurl )
			->with('ajaxsource',URL::to($this->ajaxsource) )
			->with('ajaxdel',URL::to($this->delurl) )
            ->with('ajaxdlxl',URL::to($this->dlxl) )
			->with('crumb',$this->crumb )
            ->with('printlink', $this->printlink )
            ->with('can_add', $this->can_add )
            ->with('can_import', $this->can_import )
            ->with('can_export', $this->can_export )
            ->with('is_report',$this->is_report)
            ->with('report_action',$this->report_action)
            ->with('is_additional_action',$this->is_additional_action)
            ->with('additional_action',$this->additional_action)
            ->with('additional_filter',$this->additional_filter)
            ->with('js_additional_param', $this->js_additional_param)
            ->with('modal_sets', $this->modal_sets)
            ->with('table',$this->table_raw)
            ->with('table_dnd', $this->table_dnd)
            ->with('table_dnd_url', $this->table_dnd_url)
            ->with('table_dnd_idx', $this->table_dnd_idx)
            ->with('table_group', $this->table_group)
            ->with('table_group_field', $this->table_group_field)
            ->with('table_group_idx', $this->table_group_idx)
            ->with('table_group_collapsible', $this->table_group_collapsible)
            ->with('js_table_event', $this->js_table_event)
            ->with('column_styles', $this->column_styles)
            ->with('additional_page_data',$this->additional_page_data)
            ->with('additional_table_param',$this->additional_table_param)
            ->with('product_info_url',$this->product_info_url)
            ->with('prefix',$this->prefix)
			->with('heads',$heads )
            ->with('fields',$fields)
            ->with('start_index',$start_index)
			->with('row',$this->rowdetail );


	}

    public function reportPageGenerator(){

        //$action_selection = Former::select( Config::get('kickstart.actionselection'))->name('action');

        $heads = $this->heads;
        $fields = $this->fields;

        $this->ajaxsource = (is_null($this->ajaxsource))? strtolower($this->controller_name): $this->ajaxsource;

        $this->addurl = (is_null($this->addurl))? strtolower($this->controller_name).'/add': $this->addurl;

        $this->importurl = (is_null($this->importurl))? strtolower($this->controller_name).'/import': $this->importurl;

        $this->rowdetail = (is_null($this->rowdetail))? strtolower($this->controller_name).'.rowdetail': $this->rowdetail;

        $this->delurl = (is_null($this->delurl))? strtolower($this->controller_name).'/del': $this->delurl;

        $this->newbutton = (is_null($this->newbutton))? Str::singular($this->controller_name): $this->newbutton;

        //dialog related url
        //$this->product_info_url = (is_null($this->product_info_url))? strtolower($this->controller_name).'/info': $this->product_info_url;

        $this->prefix = (is_null($this->prefix))? strtolower($this->controller_name):$this->prefix;

        $select_all = Former::checkbox()->name('All')->check(false)->id('select_all');

        // add selector and sequence columns
        $start_index = -1;
        if($this->place_action == 'both' || $this->place_action == 'first'){
            array_unshift($heads, array('Actions',array('sort'=>false,'clear'=>true,'class'=>'action')));
            array_unshift($fields, array('',array('sort'=>false,'clear'=>true,'class'=>'action')));
        }
        if($this->show_select == true){
            array_unshift($heads, array($select_all,array('sort'=>false)));
            array_unshift($fields, array('',array('sort'=>false)));
        }else{
            $start_index = $start_index + 1;
        }
        array_unshift($heads, array('#',array('sort'=>false)));

        array_unshift($fields, array('',array('sort'=>false)));

        // add action column
        if($this->place_action == 'both'){
            array_push($heads,
                array('Actions',array('search'=>false,'sort'=>false,'clear'=>true,'class'=>'action'))
            );
            array_push($fields,
                array('',array('search'=>false,'sort'=>false,'clear'=>true,'class'=>'action'))
            );
        }

        $disablesort = array();

        for($s = 0; $s < count($heads);$s++){
            if($heads[$s][1]['sort'] == false){
                $disablesort[] = $s;
            }
        }

        $disablesort = implode(',',$disablesort);

        /* additional features */

/*

        $this->dlxl = (is_null($this->dlxl))? strtolower($this->controller_name).'/dlxl': $this->dlxl;


        $this->printlink = (is_null($this->printlink) || $this->printlink == '')? strtolower($this->controller_name).'/print': $this->printlink;

        $this->pdflink = (is_null($this->pdflink) || $this->pdflink == '')? strtolower($this->controller_name).'/genpdf': $this->pdflink;

        $this->xlslink = (is_null($this->xlslink) || $this->xlslink == '')? strtolower($this->controller_name).'/genxls': $this->xlslink;
        /*
        if($this->report_entity == false){

        }else{
            $this->report_entity = (is_null($this->report_entity) || $this->report_entity == '')? strtolower($this->controller_name): $this->report_entity;

            if($this->doc_number == false){
                $sequencer = new Sequence();
                $this->doc_number = $sequencer->getNewId($this->report_entity);
            }
        }
        */


/*

            $html = View::make($this->report_view)
                ->with('title',$this->title )
                ->with('report_data', $this->report_data)
                ->with('newbutton', $this->newbutton )
                ->with('disablesort',$disablesort )
                ->with('addurl',$this->addurl )
                ->with('importurl',$this->importurl )
                ->with('ajaxsource',URL::to($this->ajaxsource) )
                ->with('ajaxdel',URL::to($this->delurl) )
                ->with('ajaxdlxl',URL::to($this->dlxl) )
                ->with('crumb',$this->crumb )
                ->with('printlink', $this->printlink )
                ->with('pdflink', $this->pdflink )
                ->with('xlslink', $this->xlslink )
                ->with('can_add', $this->can_add )
                ->with('is_report',$this->is_report)
                ->with('report_action',$this->report_action)
                ->with('doc_number',$this->doc_number)
                ->with('is_additional_action',$this->is_additional_action)
                ->with('additional_action',$this->additional_action)
                ->with('additional_filter',$this->additional_filter)
                ->with('js_additional_param', $this->js_additional_param)
                ->with('modal_sets', $this->modal_sets)
                ->with('tables',$this->table_raw)
                ->with('table_dnd', $this->table_dnd)
                ->with('table_dnd_url', $this->table_dnd_url)
                ->with('table_dnd_idx', $this->table_dnd_idx)
                ->with('table_group', $this->table_group)
                ->with('table_group_field', $this->table_group_field)
                ->with('table_group_idx', $this->table_group_idx)
                ->with('table_group_collapsible', $this->table_group_collapsible)
                ->with('js_table_event', $this->js_table_event)
                ->with('column_styles', $this->column_styles)
                ->with('additional_page_data',$this->additional_page_data)
                ->with('additional_table_param',$this->additional_table_param)
                ->with('product_info_url',$this->product_info_url)
                ->with('prefix',$this->prefix)
                ->with('heads',$heads )
                ->with('fields',$fields)
                ->with('start_index',$start_index)
                ->with('row',$this->rowdetail )
                ->with('pdf',$this->pdf);

        /*
        PDF::loadHTML($html->render())->setPaper('a4')
                 ->setOrientation('landscape')
                 ->setOption('margin-bottom', 0)
                 ->save($this->report_file_path.$this->report_file_name);
        */
                 /*
        if($this->report_file_name){


            file_put_contents($this->report_file_path.$this->report_file_name, $html);

            $sd = new Document();
            $sd->timestamp = new MongoDate();
            $sd->type = $this->report_type;
            $sd->fullpath = $this->report_file_path.$this->report_file_name;
            $sd->filename = $this->report_file_name;
            $sd->creator_id = Auth::user()->_id;
            $sd->creator_name = Auth::user()->fullname;
            $sd->save();

        }


        if($this->pdf == true){

            $html->render();

            $snappy = App::make('snappy.pdf');

            return PDF::loadHTML($html)->setPaper('a4')
                     ->setOrientation('landscape')->setOption('margin-bottom', 0)->stream($this->report_file_name);

        }

        if($this->xls == true){

            $tables = $this->table_raw;

            $heads = $this->additional_filter;

            Excel::create($this->report_file_name, function($excel) use($tables, $heads){

                $excel->sheet('New sheet', function($sheet) use($tables, $heads){

                    $xls_view = 'tables.xls';

                    $sheet->loadView($xls_view)
                        ->with('heads',$heads )
                        ->with('tables',$tables);

                });

            })->download('xls');

        }else{

            return $html;

        }

    }


	public function tableResponder()
	{

        date_default_timezone_set('Asia/Jakarta');

		$fields = $this->fields;

        $count_all = 0;
        $count_display_all = 0;

		//print_r($fields);

		//array_unshift($fields, array('select',array('kind'=>false)));
		array_unshift($fields, array('seq',array('kind'=>false)));
        if($this->place_action == 'both' || $this->place_action == 'first'){
            array_unshift($fields, array('action',array('kind'=>false)));
        }

		$pagestart = Input::get('iDisplayStart');
		$pagelength = Input::get('iDisplayLength');

		$limit = array($pagelength, $pagestart);

        $defsort = 1;
        $defdir = -1;

        $idx = 0;
        $q = array();

        $hilite = array();
        $hilite_replace = array();


        //$model = $this->model;

        //$table = $emodel->getTable();

        $model = $this->model;

        $count_all = $model->count();

        $model = $this->SQL_additional_query($model);

        //$model = $this->SQL_make_join($model);

        //$comres = $this->SQLcompileSearch($fields, $model);

        $comres = $this->MongoCompileSearch($fields, $model);

        $model = $comres['model'];
        $q = $comres['q'];
		//print_r($q);


		/* first column is always sequence number, so must be omitted */


/*
		$fidx = Input::get('iSortCol_0') - 1;

		$fidx = ($fidx == -1 )?0:$fidx;

        if(Input::get('iSortCol_0') == 0){
            $sort_col = $this->def_order_by;

            $sort_dir = $this->def_order_dir;
        }else{
            $sort_col = $fields[$fidx][0];

            $sort_dir = Input::get('sSortDir_0');

        }


		/*
		if(count($q) > 0){
			$results = $model->skip( $pagestart )->take( $pagelength )->orderBy($sort_col, $sort_dir )->get();
			$count_display_all = $model->count();
		}else{
			$results = $model->find(array(),array(),array($sort_col=>$sort_dir),$limit);
			$count_display_all = $model->count();
		}
		*/

        //$model->where('docFormat','picture');


/*
        $count_display_all = $model->count();

        $this->aux_data = $this->SQL_before_paging($model);

        if($this->no_paging == true){
            $results = $model->orderBy($sort_col, $sort_dir )->get();
        }else{
            $results = $model->skip( $pagestart )->take( $pagelength )->orderBy($sort_col, $sort_dir )->get();
        }

        //print_r($results->toArray());

		$aadata = array();

		$form = $this->form;

		$counter = 1 + $pagestart;


        //$count_display_all = count($results);


		foreach ($results as $doc) {

			$extra = $doc;

			//$select = Former::checkbox('sel_'.$doc['_id'])->check(false)->id($doc['_id'])->class('selector');
            $actionMaker = $this->makeActions;

			$actions = $this->$actionMaker($doc);

			$row = array();

			$row[] = $counter;

            if($this->show_select == true){
                //$sel = Former::checkbox('sel_'.$doc['_id'])->check(false)->label(false)->id($doc['_id'])->class('selector')->__toString();
                //$sel = '<input type="checkbox" name="sel_'.$doc['_id'].'" id="'.$doc['_id'].'" value="'.$doc['_id'].'" class="selector" />';
                $sel = '<input type="checkbox" name="sel_[]" id="'.$doc['_id'].'" value="'.$doc['_id'].'" class="selector" />';
                $row[] = $sel;
            }

            if($this->place_action == 'both' || $this->place_action == 'first'){
                $row[] = $actions;
            }


			foreach($fields as $field){
				if($field[1]['kind'] != false && $field[1]['show'] == true){

					$fieldarray = explode('.',$field[0]);
					if(is_array($fieldarray) && count($fieldarray) > 1){
						$fieldarray = implode('\'][\'',$fieldarray);
						$cstring = '$label = (isset($doc[\''.$fieldarray.'\']))?true:false;';
						eval($cstring);
					}else{
						$label = (isset($doc[$field[0]]))?true:false;
					}


					if($label){

						if( isset($field[1]['callback']) && $field[1]['callback'] != ''){
							$callback = $field[1]['callback'];
							$row[] = $this->$callback($doc, $field[0]);
						}else{
							if($field[1]['kind'] == 'datetime' || $field[1]['kind'] == 'datetimerange'){
                                if($doc[$field[0]] instanceof MongoDate){
                                    $rowitem = date('d-m-Y H:i:s',$doc[$field[0]]->sec);
                                }elseif ($doc[$field[0]] instanceof Date) {
                                    $rowitem = date('d-m-Y H:i:s',$doc[$field[0]]);
                                }else{
                                    //$rowitem = $doc[$field[0]];
                                    if(is_array($doc[$field[0]])){
                                        $rowitem = date('d-m-Y H:i:s', time() );
                                    }else{
                                        $rowitem = date('d-m-Y H:i:s',strtotime($doc[$field[0]]) );
                                    }
                                }
							}elseif($field[1]['kind'] == 'date' || $field[1]['kind'] == 'daterange'){
                                if($doc[$field[0]] instanceof MongoDate){
                                    $rowitem = date('d-m-Y',$doc[$field[0]]->sec);
                                }elseif ($doc[$field[0]] instanceof Date) {
                                    $rowitem = date('d-m-Y',$doc[$field[0]]);
                                }else{
                                    //$rowitem = $doc[$field[0]];
                                    $rowitem = date('d-m-Y',strtotime($doc[$field[0]]) );
                                }
							}elseif($field[1]['kind'] == 'currency'){
								$num = (double) $doc[$field[0]];
								$rowitem = number_format($num,2,',','.');
							}else{
								$rowitem = $doc[$field[0]];
							}

							if(isset($field[1]['attr'])){
								$attr = '';
								foreach ($field[1]['attr'] as $key => $value) {
									$attr .= $key.'="'.$value.'" ';
								}
								$row[] = '<span '.$attr.' >'.$rowitem.'</span>';
							}else{
								$row[] = $rowitem;
							}

						}


					}else{
						$row[] = '';
					}
				}
			}

            if($this->place_action == 'both'){
                $row[] = $actions;
            }

			$row['extra'] = $extra;

			$aadata[] = $row;

			$counter++;
		}

        $aadata = $this->rows_post_process($aadata, $this->aux_data);

        $sEcho = (int) Input::get('sEcho');

		$result = array(
			'sEcho'=>  $sEcho,
			'iTotalRecords'=>$count_all,
			'iTotalDisplayRecords'=> (is_null($count_display_all))?0:$count_display_all,
			'aaData'=>$aadata,
			'qrs'=>$q,
			'sort'=>array($sort_col=>$sort_dir)
		);

		return Response::json($result);
	}

    public function __SQLtableResponder()
    {
        set_time_limit(0);

        $fields = $this->fields;

        $count_all = 0;
        $count_display_all = 0;

        //print_r($fields);

        //print 'print '.$this->print.' paging '.$this->no_paging;


        //array_unshift($fields, array('select',array('kind'=>false)));
        array_unshift($fields, array('seq',array('kind'=>false)));

        //if($this->show_select == true){
        //    array_unshift($fields, array('select',array('kind'=>false)));
        //}

        if($this->place_action == 'both' || $this->place_action == 'first'){
            array_unshift($fields, array('action',array('kind'=>false)));
        }

        $pagestart = Input::get('iDisplayStart');
        $pagelength = Input::get('iDisplayLength');

        $limit = array($pagelength, $pagestart);

        $defsort = 1;
        $defdir = -1;

        $idx = 0;
        $q = array();

        $hilite = array();
        $hilite_replace = array();


        //$model = $this->model;

        //$table = $emodel->getTable();

        $model = DB::connection($this->sql_connection)->table($this->sql_table_name);

        $model = $this->SQL_additional_query($model);

        //$model = $this->SQL_make_join($model);

        for($i = 0;$i < count($fields);$i++){
            $idx = $i;

            //print_r($fields[$i]);

            $field = $fields[$i][0];
            $type = $fields[$i][1]['kind'];


            $qval = '';

            $sfields = explode('.',$field);
            $sub = '';
            if(count($sfields) > 1){
                $sub = $sfields[0];
                $subfield = $sfields[1];
            }

            if(Input::get('sSearch_'.$i))
            {
                if( $type == 'text'){
                    if($fields[$i][1]['query'] == 'like'){
                        $pos = $fields[$i][1]['pos'];
                        if($pos == 'both'){
                            //$model->whereRegex($field,'/'.Input::get('sSearch_'.$idx).'/i');
                            $model = $model->where($field,'like','%'.Input::get('sSearch_'.$idx).'%');
                            /*
                            if($sub == ''){
                                $model = $model->where($field,'like','%'.Input::get('sSearch_'.$idx).'%');
                            }else{
                                $model = $model->whereHas($sub, function($q) use ($subfield, $idx) {
                                    $q->where($subfield,'like','%'.Input::get('sSearch_'.$idx).'%');
                                });
                            }
                            */

}