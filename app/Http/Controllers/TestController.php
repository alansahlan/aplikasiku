<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestController extends AdminController
{
protected $response;
public function __construct($response){
	$this->response = $response;
} 
 public function getIndex(){
 	return $this->response->make('Hello World');
 }
}
