<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\aplikasi;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect; 
use App\Kamar;
use App\Reservasi;
use DB;
class RoomController extends Controller
{
    public $kamarlist;

    public function show(){
        $kamars ='kamar';
        $kamar_list = Kamar::all();
        $jumlah_kamar = $kamar_list->count();

        $kamar_vvip = Kamar::where('kelas_kamar','VVIP');
        $kamarvvip = $kamar_vvip->count();

        $kamar_vip = Kamar::where('kelas_kamar','VIP');
        $kamarvip = $kamar_vip->count();

        $kamar_kelas1 = Kamar::where('kelas_kamar','Kelas 1');
        $kamarkelas1 = $kamar_kelas1->count();

        $kamar_kelas2 = Kamar::where('kelas_kamar','Kelas II');
        $kamarkelas2 = $kamar_kelas2->count();

        $kamar_kelas3 = Kamar::where('kelas_kamar','Kelas III');
        $kamarkelas3 = $kamar_kelas3->count();
        
        return view('check.room',compact('kamars','kamar_list','jumlah_kamar','kamarvvip','kamarvip','kamarkelas1','kamarkelas2','kamarkelas3'));

    }

    public function reservroom($id){

        $kamar ='kamar';
        $kamar_list = Kamar::findOrFail($id);
        return view('rs.reservasi', compact('kamar','kamar_list'));
    }

    public function showlist(){
        $kamar_list = Kamar::all();
        return view('rs.listroom')->with('kamar',$kamar_list);
    }

    public function reserv(){

    	$data = array(
            'nama_kamar' =>Input::get('nama_kamar'),
    		'no_ktp' => Input::get('no_ktp'),
    		'nama_pasien' => Input::get('nama_pasien'),
    		'alamat' => Input::get('alamat'),
    		'no_hp' => Input::get('no_hp'),
    		'rujukan' => Input::get('rujukan'),
    		'tgl_reservasi' => Input::get('tgl_reservasi'),
    		'keterangan' => Input::get('keterangan'),
    		);
    	DB::table('reservasi')->insert($data);
    	return Redirect::to('listroom')->with('message','Mohon Tunggu 1x24');
    }

    public function showreservasi(){
        $reservasi ='reservasi';

        $data = Reservasi::all();

        return view('reservasi.showreservasi')->with('reservasi',$data);
    }
    
}
