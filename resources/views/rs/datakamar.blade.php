@extends('layouts.admin')
 
@section('content')
<div class="container">
	<div class="row">
	<div class="col-md-8 col-md-offset-1">
	<h3>Data Kamar</h3>
{!! Form::open(array('url'=>'data/kamar','methode'=>'post')) !!}
	<div class="form-group">
		{!! Form::label('kelas_kamar','Kelas Kamar') !!}
		{!! Form::text('kelas_kamar',null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nama_kamar','Nama Kamar') !!}
		{!! Form::text('nama_kamar',null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('lantai','Lantai') !!}
		{!! Form::text('lantai',null, array('class'=>'form-control')) !!}
	</div>
	{!! Form::token() !!}
	{!! Form::submit('Submit',null,array('class'=>'btn btn-primary')) !!}
	{!! Form::close() !!}
	</div>
</div>
</div>
@endsection
