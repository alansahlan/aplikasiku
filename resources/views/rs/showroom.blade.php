@extends('layouts.admin')

@section('content')

<div class="container">
	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<td>No</td>
				<td>Kelas Kamar</td>
				<td>Nama Kamar</td>
				<td>Lantai</td>
				<td>Action</td>
			</tr>
			<?php $no=1; ?>
			@foreach ($kamar as $kamar_list)
			<tr>
				<td>{{ $no++}}</td>
				<td>{{ $kamar_list->kelas_kamar}}</td>
				<td>{{ $kamar_list->nama_kamar}}</td>
				<td>{{ $kamar_list->lantai}}</td>
				<td><a href="#">Edit</a>Edit</td>
				<td><a href="#">Hapus</a>Hapus</td>
			</tr>
			@endforeach
		</table>

	</div>



@endsection