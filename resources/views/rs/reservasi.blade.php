@extends('layouts.member')
 
@section('content')
	
	<div class="container">
	<div class="col-md-8">
	<h3>Reservasi Kamar {{ $kamar_list->nama_kamar}}</h3>
	<hr>
	
{!! Form::open(array('url'=>'data/reservasi','method'=>'post')) !!}
	
	<input type="hidden" value="{{ $kamar_list->nama_kamar}}" name="nama_kamar"/>
	<div class="form-group">
		{!! Form::label('Nomor Identitas') !!}
		{!! Form::text('no_ktp',null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('Nama Pasien') !!}
		{!! Form::text('nama_pasien',null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label ('Alamat') !!}
		{!! Form::textarea('alamat', null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('Nomor HP') !!}
		{!! Form::text('no_hp', null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('Rujukan Dokter') !!}
		{!! Form::text('rujukan', null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('Tanggal Reservasi') !!}
		{!! Form::date('tgl_reservasi',null, array('class'=>'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('Keterangan') !!}
		{!! Form::textarea('keterangan',null, array('class'=>'form-control'))!!}
	</div>
	
	{!! Form::token() !!}	
	<button type="submit" id="pesan" onclick="confirm()" class="btn btn-primary btn-lg">
  Pesan
</button>

<!-- Modal -->

	
{!! Form::close() !!}
<br/>
</br>
	</div>
</div>
<script>
	function confirm(){
		window.confirm("Pesanan Anda Akan Kami proses");
		if(result == true){
			alert("Terima kasih telah memesan");
		}else{
			alert(" Silahkan Kembali kemenu Reservasi");
		}
	}
</script>
@endsection