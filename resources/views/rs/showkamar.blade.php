@extends('layouts.admin')

@section('content')

<style type="text/css">
	tr{
		display: table-row;
	}
	.slick-slider {
  padding-bottom: 35px;
  margin: 0;
}
.slick-slider .item > * {
  margin: 0;
}
.slick-basic {
  position: relative;
  overflow: hidden;
}
.slick-basic .item {
  margin: 3px;
}
.slick-dots {
  height: 25px;
  bottom: 0;
  margin: 0;
}
.slick-dots li {
  margin: 0;
}
.slick-dots li button:before {
  font-size: 14px;
  color: #e0e0e0;
  opacity: 1;
}
.slick-dots li.slick-active button:before {
  color: #26a69a;
}
.label {
    padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 4px;
    display: inline-block;
}

</style>

<div class="container">
	<h4>Ketersediaan Tempat Tidur</h4>
	<hr>
	@foreach($kamar as $kamar_list)
    <div class="col-md-12">
            <div class="item">
              <div class="panel panel-default relative">
                <div class="ribbon-heading" style="text-align: left;"><strong><h4>{{ $kamar_list->nama_kamar}}</h4></strong></div>
                <div class="cover hover overlay margin-none">
                  <img src="{{ URL::to('/')}}/images/vip1.jpg" alt="location" class="img-responsive" style="float: left;" width="180px;" height="150px;" />
                  <a href="front-property.html" class="overlay overlay-full overlay-bg-black overlay-hover">
                    <span class="v-center">
                        <span class="btn btn-circle btn-white"><i class="fa fa-eye"></i></span>
                    </span>
                  </a>
                </div>
                <div class="panel-body">
                  <h4> Kelas : {{ $kamar_list->kelas_kamar}}</h4>
                  <p class="small">
                    <span class="fa fa-fw fa-star text-yellow-800"></span>
                    <span class="fa fa-fw fa-star text-yellow-800"></span>
                    <span class="fa fa-fw fa-star text-yellow-800"></span>
                    <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                    <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                  </p>
                  <h4>Status : Tersedia</h4>
                  <h4> Fasilitas : Kamar Tidur 1, Tv, dan Kamar Mandi</h4>
                  <br/>
                  <br/>
                  <i class="small fa fa-fw icon-home-fill-1" data-toggle="tooltip" data-title="Agency"></i>
                  <a href="{{URL('/reservasi')}}" class="btn btn-primary absolute bottom left">Reservasi<i class="fa fa-eye"></i></a>
                </div>
              </div>
            </div>
            </div>
        @endforeach
        <!--
-->
</div>



@endsection