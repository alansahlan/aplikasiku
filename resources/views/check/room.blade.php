@extends('layouts.member')

@section('content')
<br/>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Cek Ketersediaan Kamar</h3></div>

                <div class="panel-body">
                <table class="table table-striped">
                <tr>
                    <th>Nama Kamar</th>
                    <td>Jumlah Kamar</td>
                </tr>
                <tr>
                    <th>Kamar VVIP</th>
                    <td><span class="label label-info">{{ $kamarvvip}}</span></td>
                </tr>
                <tr>
                    <th>Kamar VIP</th>
                    <td><span class="label label-info">{{ $kamarvip}}</span></td>
                </tr>
                <tr>
                    <th>Kamar Kelas I</th>
                    <td><span class="label label-info">{{ $kamarkelas1}}</span></td>
                </tr>
                <tr>
                    <th>Kamar Kelas II</th>
                    <td><span class="label label-info">{{ $kamarkelas2}}</span></td>
                </tr>
                <tr>
                    <th>Kamar Kelas III</th>
                    <td><span class="label label-info">{{ $kamarkelas3}}</span></td>
                </tr>
                </table>
                
                <br/>
              <h4>  <a class="btn btn-primary" href="{{url('/listroom')}}">Cek Kamar</a>
               <a class="btn btn-success"> Jumlah Kamar Tersedia :{{ $jumlah_kamar}}</a></h4>
                
               
</div>

@stop