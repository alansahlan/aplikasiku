@extends('layouts.admin')

@section('content')

<div class="container">
	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<td>No</td>
				<td>Nama Kamar</td>
				
				<td>Nama Pasien</td>
				<td>Alamat</td>
				<td>No Telepon</td>
				<td>Rujukan</td>
				<td>Tanggal Reservasi</td>
				<td>Keterangan</td>
			</tr>
			<?php $no=1; ?>
			@foreach ($reservasi as $data)
			<tr>
				<td>{{ $no++}}</td>
				<td>{{ $data->nama_kamar}}</td>
				
				<td>{{ $data->nama_pasien}}</td>
				<td>{{ $data->alamat}}</td>
				<td>{{ $data->no_hp}}</td>
				<td>{{ $data->rujukan}}</td>
				<td>{{ $data->tgl_reservasi}}</td>
				<td>{{ $data->keterangan}}</td>
				<td><a href="#">Edit</a>Edit</td>
				<td><a href="#">Hapus</a>Hapus</td>
			</tr>
			@endforeach
		</table>

	</div>



@endsection